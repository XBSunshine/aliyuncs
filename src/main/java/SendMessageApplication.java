import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;

public class SendMessageApplication {

    public static void main(String[] args) {
        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", "LTAIXSYTf8Raommx", "nhgVaQqFviZQ01bvouyHnBahKozEj4");//自己的accesskeyid和secret
        CommonRequest request = new CommonRequest();
        request.setSysMethod(MethodType.POST);
        request.setSysDomain("dysmsapi.aliyuncs.com");
        request.setSysAction("SendSms");
        request.setSysVersion("2017-05-25");
        request.putQueryParameter("PhoneNumbers","15241799395");//手机号
        request.putQueryParameter("SignName","国际物流云商系统");//签名
        request.putQueryParameter("TemplateCode","SMS_112485287");//模板
        request.putQueryParameter("TemplateParam","{code:'好喜欢你'}");//变量json格式
        DefaultAcsClient client = new DefaultAcsClient(profile);
        try {
            CommonResponse response = client.getCommonResponse(request);
            System.out.println(response.getData());
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
